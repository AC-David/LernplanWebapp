package lernplan.webapp;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/home")
public class MainServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String name = req.getParameter("name");
        resp.setContentType("text/xml");
        if(name != null){
            resp.getWriter().write(String.format("<name>Hello %s</name>", name));
        }else{
            resp.getWriter().write(String.format("<msg>Junge, Name angeben</msg>"));
        }


    }
}
